FROM alpine:3.13

RUN echo "http://dl-cdn.alpinelinux.org/alpine/v3.13/main" > /etc/apk/repositories && \
    echo "http://dl-cdn.alpinelinux.org/alpine/v3.13/community" >> /etc/apk/repositories && \
    apk update && \
    apk add make openssl curl ansible k3s && \
    ansible-galaxy collection install community.kubernetes && \
    mkdir -p /drone/src /root/.kube

RUN wget https://get.helm.sh/helm-v3.5.3-linux-amd64.tar.gz -O /var/cache/misc/helm.tar.gz && \
    cd /var/cache/misc/ && tar xvzf /var/cache/misc/helm.tar.gz && \
    mv /var/cache/misc/linux-amd64/helm /usr/bin/helm && \
    rm -rf /var/cache/misc/*

RUN apk add yarn git

WORKDIR /drone/src
